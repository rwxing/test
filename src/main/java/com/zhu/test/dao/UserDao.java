package com.zhu.test.dao;

import java.util.List;

import com.zhu.test.entity.User;

/**
 * user dao层接口
 * 
 * @author zhu
 *
 */
public interface UserDao {
	/**
	 * 创建或修改用户
	 * 
	 * @param user
	 * @return
	 */
	User saveOrUpdateUser(User user);

	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	void deleteUser(Long id);

	/**
	 * 根据id查询用户
	 * 
	 * @param id
	 * @return
	 */
	User getUserById(Long id);

	/**
	 * 查询用户列表
	 * 
	 * @return
	 */
	List<User> listUsers();

}
