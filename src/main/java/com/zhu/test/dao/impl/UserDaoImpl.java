package com.zhu.test.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Repository;

import com.zhu.test.dao.UserDao;
import com.zhu.test.entity.User;
/**
 * user dao层实现
 * @author zhu
 *
 */
@Repository
public class UserDaoImpl implements UserDao {
	//用来计数的
    private static AtomicLong counter = new AtomicLong();
	// 用来保存user的map
	private final ConcurrentMap<Long, User> userMap = new ConcurrentHashMap<>();
	
	@Override
	public User saveOrUpdateUser(User user) {
		Long id = user.getId();
		if(id == null) {//save
			id = counter.incrementAndGet();
			user.setId(id);
		}
		this.userMap.put(id, user);
		return user;
	}

	@Override
	public void deleteUser(Long id) {
		this.userMap.remove(id);

	}

	@Override
	public User getUserById(Long id) {
		return this.userMap.get(id);
	}

	@Override
	public List<User> listUsers() {
		return new ArrayList<User>(this.userMap.values());
	}

}
